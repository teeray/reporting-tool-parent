export class DbUser {
    public _username: String = '';
    public _password: String = '';

    get username(): String {
        return this._username;
    }

    set username(value: String) {
        this._username = value;
    }

    get password(): String {
        return this._password;
    }

    set password(value: String) {
        this._password = value;
    }

    toJSON(): string {
        const obj = Object.assign(this);
        const keys = Object.keys(this.constructor.prototype);
        obj.toJSON = undefined;
        return JSON.stringify(obj, keys);
    }
}
