import { TestBed, inject } from '@angular/core/testing';

import { ExtensionDetailedDataService } from './extension-detailed-data.service';

describe('ExtensionDetailedDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExtensionDetailedDataService]
    });
  });

  it('should be created', inject([ExtensionDetailedDataService], (service: ExtensionDetailedDataService) => {
    expect(service).toBeTruthy();
  }));
});
