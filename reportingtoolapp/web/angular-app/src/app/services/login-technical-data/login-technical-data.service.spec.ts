import { TestBed, inject } from '@angular/core/testing';

import { LoginTechnicalDataService } from './login-technical-data.service';

describe('LoginTechnicalDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoginTechnicalDataService]
    });
  });

  it('should be created', inject([LoginTechnicalDataService], (service: LoginTechnicalDataService) => {
    expect(service).toBeTruthy();
  }));
});
