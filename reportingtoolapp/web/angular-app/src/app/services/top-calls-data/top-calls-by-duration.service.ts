import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Extension} from '../../models/Extension';
import {Observable} from 'rxjs/index';

@Injectable()
export class TopCallsByDurationService {

    constructor(private http: HttpClient) {
    }

    getTopCallsByDuration(howManyCalls: number): Observable<Extension[]> {
        return this.http.get<Extension[]>('http://localhost:8081/reports/top-calls/duration?howManyCalls=' + howManyCalls);
    }
}
