import { TestBed, inject } from '@angular/core/testing';

import { PinCodeDataService } from './pin-code-data.service';

describe('PinCodeDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PinCodeDataService]
    });
  });

  it('should be created', inject([PinCodeDataService], (service: PinCodeDataService) => {
    expect(service).toBeTruthy();
  }));
});
