import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtensionSummaryComponent } from './extension-summary.component';

describe('ExtensionSummaryComponent', () => {
  let component: ExtensionSummaryComponent;
  let fixture: ComponentFixture<ExtensionSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtensionSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtensionSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
