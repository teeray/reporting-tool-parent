import {Component, OnInit, ViewChild} from '@angular/core';
import {TopCallsComponent} from '../top-calls/top-calls/top-calls.component';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  allowInternal = false;
  @ViewChild(TopCallsComponent) topCalls: TopCallsComponent;

  constructor() { }

  ngOnInit() {
  }

  getTopCallsByDuration(howMany: number) {
        this.topCalls.getTopCallsByDuration(howMany);
  }

}
