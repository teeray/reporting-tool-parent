
import {merge, of, Observable} from 'rxjs';
import {DataSource} from '@angular/cdk/collections';
import {MatPaginator, MatSort} from '@angular/material';
import {Extension} from '../models/Extension';
import {map} from 'rxjs/operators';

export class ExtensionSummaryDataSource extends DataSource<any> {

    constructor(private extensions: Extension[], private sort: MatSort, private paginator: MatPaginator) {
        super();
    }

    connect(): Observable<any[]> {
        if (this.extensions === null || this.extensions === []) {
            this.extensions = [];

            return of(this.extensions);
        } else {
            const dataChanges = [
                this.extensions,
                this.sort.sortChange,
                this.paginator.page
            ];

            return merge(...dataChanges).pipe(
                map(() => {
                    const sortedData = this.sortData();
                    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
                    return sortedData.splice(startIndex, this.paginator.pageSize);
                }));
        }
    }

    sortData(): any[] {
        const sortedData = this.extensions.slice();

        if (!this.sort.active || this.sort.direction === '') {
            return sortedData;
        }

        return sortedData.sort((a, b) => {
            let isAsc = this.sort.direction === 'asc';

            switch (this.sort.active) {
                case 'clid':
                    return this.compare(a.clid, b.clid, isAsc);
                case 'src':
                    return this.compare(+a.src, +b.src, isAsc);
                case 'duration':
                    return this.compare(+a.duration, +b.duration, isAsc);
                case 'cost':
                    return this.compare(+a.cost, +b.cost, isAsc);
                case 'numOfCalls':
                    return this.compare(+a.numOfCalls, +b.numOfCalls, isAsc);
                default:
                    return 0;
            }
        });
    }

    compare(a, b, isAsc) {
        return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
    }

    disconnect() {
    }
}
