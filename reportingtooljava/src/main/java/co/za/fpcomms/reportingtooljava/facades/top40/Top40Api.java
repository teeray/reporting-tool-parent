package co.za.fpcomms.reportingtooljava.facades.top40;

import co.za.fpcomms.reportingtooljava.entities.CdrBaseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

public interface Top40Api {

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/reports/top-calls/duration")
    List<CdrBaseEntity> sortByDuration(@PathVariable(value = "howManyCalls") Long howManyCalls);

}
