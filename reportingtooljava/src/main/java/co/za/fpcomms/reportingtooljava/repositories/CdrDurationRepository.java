//package co.za.fpcomms.reportingtooljava.repositories;
//
//import co.za.fpcomms.reportingtooljava.entities.CdrDurationEntity;
//import org.springframework.data.repository.CrudRepository;
//import org.springframework.stereotype.Repository;
//
//import java.util.List;
//
//@Repository
//public interface CdrDurationRepository extends CrudRepository<CdrDurationEntity, Long>{
//
//    List<CdrDurationEntity> findAll();
//
//}
