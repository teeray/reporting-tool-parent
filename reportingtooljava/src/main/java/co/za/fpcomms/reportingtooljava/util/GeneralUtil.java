package co.za.fpcomms.reportingtooljava.util;

import co.za.fpcomms.reportingtooljava.entities.CdrBaseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GeneralUtil {

//    public boolean mustIncludeThis() {
//        return true;
//    }

    public void filterClid(CdrBaseEntity ext) {
        String unfilteredClid = ext.getClid();
        if (unfilteredClid.contains("\"")) {
            //Get CLID between quotes
            int firstQuote = unfilteredClid.indexOf('"');
            int secondQuote = unfilteredClid.lastIndexOf('"');

            String filteredClid = unfilteredClid.substring(firstQuote + 1, secondQuote);
            ext.setClid(filteredClid);
        }
    }

//    public String filterClid(String unfilteredClid) {
//                if (unfilteredClid.contains("\"")) {
//                    //Get CLID between quotes
//                    int firstQuote = unfilteredClid.indexOf('"');
//                    int secondQuote = unfilteredClid.lastIndexOf('"');
//
//                    String filteredClid = unfilteredClid.substring(firstQuote + 1, secondQuote);
//                    return filteredClid;
//                } else if (unfilteredClid.equals("")) {
//                    return "";
//                } else return "RB:";
//    }

    public boolean isInternal(CdrBaseEntity ext, boolean allowInternal) {
        if (!allowInternal && ((ext.getSrc().length() < 8) && (ext.getDestination().length() < 8))) {
            return true;
        } else return false;
    }

    public boolean isInteger(String duration) {
        try {
            Integer.parseInt(duration);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    public void updateTimeFormat(CdrBaseEntity ext){
        String convertedTime = convertSecondsToTime(ext.getDuration());
        ext.setDuration(convertedTime);

        String[] arr = convertedTime.split(":");
        int hours = Integer.parseInt(arr[0]);
        int minutes = Integer.parseInt(arr[1]);
        ext.setCost(String.valueOf((hours * 60) + minutes));
    }

    public void updateCellNo(CdrBaseEntity ext) {
        // src
        String oldSrc = ext.getSrc();
        String newSrc;
        if ((oldSrc.length() > 7) && oldSrc.startsWith("9")) {
            newSrc = oldSrc.substring(1, oldSrc.length());
            ext.setSrc(newSrc);
        }

        // destination
        String oldDst = ext.getDestination();
        String newDst;
        if ((oldDst.length() > 7) && oldDst.startsWith("9")) {
            newDst = oldDst.substring(1, oldDst.length());
            ext.setDestination(newDst);
        }
    }

    // CONVERT TIME TO HH:MM:SS
    public String convertSecondsToTime(String duration) {
        int totalDuration = Integer.parseInt(duration);

        int hours = totalDuration / 3600;
        int mins = (totalDuration % 3600) / 60;
        int secs = totalDuration % 60;

        return String.format("%02d:%02d:%02d", hours, mins, secs);
    }

}
