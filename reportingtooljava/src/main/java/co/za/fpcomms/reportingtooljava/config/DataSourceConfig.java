package co.za.fpcomms.reportingtooljava.config;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import javax.sql.DataSource;

@Configuration
@RefreshScope
public class DataSourceConfig {

    @Resource
    private DbDetails env;

    @Bean
    @RefreshScope
    public DataSource dataSource(){

        System.out.println(env.getDbPassword());

        return DataSourceBuilder.create()
                .driverClassName("com.mysql.jdbc.Driver")
                .username(env.getDbUserName())
                .password(env.getDbPassword())
                .url(env.getDbUrl())
                .build();
    }

}
