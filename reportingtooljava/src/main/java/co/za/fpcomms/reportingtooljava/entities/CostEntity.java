package co.za.fpcomms.reportingtooljava.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "cost")
public class CostEntity {

    @Id
    @Column(name = "id", nullable = false)
    private int id;

    @Column(name = "Destination")
    private String destination;

    @Column(name = "Destination_Group")
    private String destinationGroup;

    @Column(name = "Country")
    private String country;

    @Column(name = "Description")
    private String description;

    @Column(name = "Off_peak_period")
    private String offPeakPeriod;

    @Column(name = "First_interval")
    private String firstInterval;

    @Column(name = "Next_interval")
    private String nextInterval;

    @Column(name = "First_price")
    private String firstPrice;

    @Column(name = "Next_price")
    private String nextPrice;

    @Column(name = "Off_peak_first_interval")
    private String offPeakFirstInterval;

    @Column(name = "Off_peak_next_interval")
    private String offPeakNextInterval;

    @Column(name = "Off_peak_first_price")
    private String offPeakFirstPrice;

    @Column(name = "Off_peak_next_price")
    private String offPeakNextPrice;

    @Column(name = "Second_off_peak_first_price")
    private String secondOffPeakFirstPrice;

    @Column(name = "Second_off_peak_next_price")
    private String secondOffPeakNextPrice;

    @Column(name = "Payback_rate")
    private String paybackRate;

    @Column(name = "Forbidden")
    private String forbidden;

    @Column(name = "Hidden")
    private String hidden;

    @Column(name = "Discontinued")
    private String discontinued;

    @Column(name = "Effective_from")
    private String effectiveFrom;

    @Column(name = "Formula")
    private String formula;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDestinationGroup() {
        return destinationGroup;
    }

    public void setDestinationGroup(String destinationGroup) {
        this.destinationGroup = destinationGroup;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getOffPeakPeriod() {
        return offPeakPeriod;
    }

    public void setOffPeakPeriod(String offPeakPeriod) {
        this.offPeakPeriod = offPeakPeriod;
    }

    public String getFirstInterval() {
        return firstInterval;
    }

    public void setFirstInterval(String firstInterval) {
        this.firstInterval = firstInterval;
    }

    public String getNextInterval() {
        return nextInterval;
    }

    public void setNextInterval(String nextInterval) {
        this.nextInterval = nextInterval;
    }

    public String getFirstPrice() {
        return firstPrice;
    }

    public void setFirstPrice(String firstPrice) {
        this.firstPrice = firstPrice;
    }

    public String getNextPrice() {
        return nextPrice;
    }

    public void setNextPrice(String nextPrice) {
        this.nextPrice = nextPrice;
    }

    public String getOffPeakFirstInterval() {
        return offPeakFirstInterval;
    }

    public void setOffPeakFirstInterval(String offPeakFirstInterval) {
        this.offPeakFirstInterval = offPeakFirstInterval;
    }

    public String getOffPeakNextInterval() {
        return offPeakNextInterval;
    }

    public void setOffPeakNextInterval(String offPeakNextInterval) {
        this.offPeakNextInterval = offPeakNextInterval;
    }

    public String getOffPeakFirstPrice() {
        return offPeakFirstPrice;
    }

    public void setOffPeakFirstPrice(String offPeakFirstPrice) {
        this.offPeakFirstPrice = offPeakFirstPrice;
    }

    public String getOffPeakNextPrice() {
        return offPeakNextPrice;
    }

    public void setOffPeakNextPrice(String offPeakNextPrice) {
        this.offPeakNextPrice = offPeakNextPrice;
    }

    public String getSecondOffPeakFirstPrice() {
        return secondOffPeakFirstPrice;
    }

    public void setSecondOffPeakFirstPrice(String secondOffPeakFirstPrice) {
        this.secondOffPeakFirstPrice = secondOffPeakFirstPrice;
    }

    public String getSecondOffPeakNextPrice() {
        return secondOffPeakNextPrice;
    }

    public void setSecondOffPeakNextPrice(String secondOffPeakNextPrice) {
        this.secondOffPeakNextPrice = secondOffPeakNextPrice;
    }

    public String getPaybackRate() {
        return paybackRate;
    }

    public void setPaybackRate(String paybackRate) {
        this.paybackRate = paybackRate;
    }

    public String getForbidden() {
        return forbidden;
    }

    public void setForbidden(String forbidden) {
        this.forbidden = forbidden;
    }

    public String getHidden() {
        return hidden;
    }

    public void setHidden(String hidden) {
        this.hidden = hidden;
    }

    public String getDiscontinued() {
        return discontinued;
    }

    public void setDiscontinued(String discontinued) {
        this.discontinued = discontinued;
    }

    public String getEffectiveFrom() {
        return effectiveFrom;
    }

    public void setEffectiveFrom(String effectiveFrom) {
        this.effectiveFrom = effectiveFrom;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }
}
