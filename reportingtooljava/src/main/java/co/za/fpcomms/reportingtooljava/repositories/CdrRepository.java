package co.za.fpcomms.reportingtooljava.repositories;

import co.za.fpcomms.reportingtooljava.entities.CdrBaseEntity;
import co.za.fpcomms.reportingtooljava.entities.EntityPK;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.ArrayList;

@Repository
public interface CdrRepository extends CrudRepository<CdrBaseEntity, EntityPK> {

    ArrayList<CdrBaseEntity> findAll();

    ArrayList<CdrBaseEntity> findByEntityPK_Src(String src);

    ArrayList<CdrBaseEntity> findByEntityPK_AccountCode(String pinCode);
}
