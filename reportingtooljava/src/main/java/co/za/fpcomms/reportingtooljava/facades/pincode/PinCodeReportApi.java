package co.za.fpcomms.reportingtooljava.facades.pincode;

import co.za.fpcomms.reportingtooljava.entities.CdrBaseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

public interface PinCodeReportApi {

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/reports/pincode")
    List<CdrBaseEntity> getByPinCode(@PathVariable(value = "pinCode") String pinCode, @PathVariable(value = "allowInternal") boolean allowInternal);

}
