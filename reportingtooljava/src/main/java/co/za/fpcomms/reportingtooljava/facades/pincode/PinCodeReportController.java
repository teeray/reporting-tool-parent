package co.za.fpcomms.reportingtooljava.facades.pincode;

import co.za.fpcomms.reportingtooljava.entities.CdrBaseEntity;
import co.za.fpcomms.reportingtooljava.services.CdrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PinCodeReportController implements PinCodeReportApi {

    @Autowired
    private CdrService cdrService;

    @Override
    public List<CdrBaseEntity> getByPinCode(String pinCode, boolean allowInternal) {
        return this.cdrService.getByPinCode(pinCode, allowInternal);
    }
}
