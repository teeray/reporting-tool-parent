package co.za.fpcomms.reportingtooljava.facades.detailed;

import co.za.fpcomms.reportingtooljava.entities.CdrBaseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

public interface ExtensionDetailedApi {

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/reports/detailed")
    List<CdrBaseEntity> searchBySrc(@PathVariable(value = "src") String src, @PathVariable(value = "allowInternal") boolean allowInternal);

}
