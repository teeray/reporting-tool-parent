package co.za.fpcomms.reportingtooljava.facades.login;

import co.za.fpcomms.reportingtooljava.config.DbDetails;
//import co.za.fpcomms.reportingtooljava.config.UpdateDbConnection;
import co.za.fpcomms.reportingtooljava.models.DbUser;
import co.za.fpcomms.reportingtooljava.models.NameAndPin;
import co.za.fpcomms.reportingtooljava.models.SetupDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.endpoint.RefreshEndpoint;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SetupDetailsController implements SetupDetailsApi {

    @Autowired
    private RefreshEndpoint refreshEndpoint;

    @Autowired
    private DbDetails db;

//    @Autowired
//    @Qualifier("test")
//    private EntityManager entityManager;

    @Override
    public void updateDetails(@RequestBody SetupDTO setupDTO){
        //this.updateProperties.updatePropertiesFile(username);
        System.out.println("---------------------" + setupDTO.getDbUser().getUsername());
        System.out.println("---------------------" + setupDTO.getDbUser().getPassword());
        NameAndPin[] nameAndPins = setupDTO.getNameAndPinCode();

        for (int x = 0; x < nameAndPins.length; x++) {
            System.out.println(nameAndPins[x].get_name());
            System.out.println(nameAndPins[x].get_pinCode());
        }

        db.setDbUserName(setupDTO.getDbUser().getUsername());
        db.setDbPassword(setupDTO.getDbUser().getPassword());

//        Properties properties = new Properties();
//        properties.setProperty("spring.datasource.url", "jdbc:mysql://127.0.0.1:3308/sys");
//        properties.setProperty("spring.datasource.username", username);
//
//        System.setProperties(properties);
//
//        applicationContext.refresh();
//        DriverManagerDataSource databaseSource = (DriverManagerDataSource)applicationContext.getBean("dataSource");
//        databaseSource.getUsername();

       // LocalContainerEntityManagerFactoryBean en = entityManager.getEntityManagerFactory();


        this.refreshEndpoint.refresh();
    }
}
