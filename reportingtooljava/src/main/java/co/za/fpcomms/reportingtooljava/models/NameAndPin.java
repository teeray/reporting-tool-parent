package co.za.fpcomms.reportingtooljava.models;

public class NameAndPin {

    private String _name;
    private String _pinCode;

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String get_pinCode() {
        return _pinCode;
    }

    public void set_pinCode(String _pinCode) {
        this._pinCode = _pinCode;
    }
}
