package co.za.fpcomms.reportingtooljava.facades.detailed;

import co.za.fpcomms.reportingtooljava.entities.CdrBaseEntity;
import co.za.fpcomms.reportingtooljava.services.CdrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ExtensionDetailedController implements ExtensionDetailedApi {

    @Autowired
    private CdrService cdrService;

    @Override
    public List<CdrBaseEntity> searchBySrc(String src, boolean allowInternal) {
        return this.cdrService.getBySrc(src, allowInternal);
    }
}
