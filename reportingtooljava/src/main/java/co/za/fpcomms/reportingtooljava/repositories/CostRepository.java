package co.za.fpcomms.reportingtooljava.repositories;

import co.za.fpcomms.reportingtooljava.entities.CostEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CostRepository extends CrudRepository<CostEntity, Long> {

    List<CostEntity> findAll();

    @Modifying
    @Query(value = "truncate sys.cost", nativeQuery = true)
    void deleteAll();

}
