//package co.za.fpcomms.reportingtooljava.facades.login;
//
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.CrossOrigin;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.multipart.MultipartFile;
//
//public interface FileUploadApi {
//
//    @CrossOrigin(origins = "http://localhost:4200")
//    @RequestMapping(value = "/login/technical/file-upload", method = RequestMethod.POST)
//    ResponseEntity<Boolean> handleFileUpload(MultipartFile file);
//
//}
