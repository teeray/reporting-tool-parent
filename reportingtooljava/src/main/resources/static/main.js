(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-login-menu></app-login-menu>\r\n\r\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/esm5/input.es5.js");
/* harmony import */ var _components_extension_extension_detail_extension_detail_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/extension/extension-detail/extension-detail.component */ "./src/app/components/extension/extension-detail/extension-detail.component.ts");
/* harmony import */ var _components_extension_extension_summary_extension_summary_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/extension/extension-summary/extension-summary.component */ "./src/app/components/extension/extension-summary/extension-summary.component.ts");
/* harmony import */ var _components_menu_menu_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/menu/menu.component */ "./src/app/components/menu/menu.component.ts");
/* harmony import */ var _services_extension_summary_data_extension_summary_data_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./services/extension-summary-data/extension-summary-data.service */ "./src/app/services/extension-summary-data/extension-summary-data.service.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _components_top_calls_top_calls_top_calls_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/top-calls/top-calls/top-calls.component */ "./src/app/components/top-calls/top-calls/top-calls.component.ts");
/* harmony import */ var _services_top_calls_data_top_calls_by_duration_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./services/top-calls-data/top-calls-by-duration.service */ "./src/app/services/top-calls-data/top-calls-by-duration.service.ts");
/* harmony import */ var _services_extension_detailed_data_extension_detailed_data_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./services/extension-detailed-data/extension-detailed-data.service */ "./src/app/services/extension-detailed-data/extension-detailed-data.service.ts");
/* harmony import */ var _components_login_technical_technical_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/login/technical/technical.component */ "./src/app/components/login/technical/technical.component.ts");
/* harmony import */ var _components_login_menu_menu_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./components/login/menu/menu.component */ "./src/app/components/login/menu/menu.component.ts");
/* harmony import */ var _services_login_technical_data_login_technical_data_service__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./services/login-technical-data/login-technical-data.service */ "./src/app/services/login-technical-data/login-technical-data.service.ts");
/* harmony import */ var _components_login_technical_simple_dialog_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./components/login/technical/simple-dialog.component */ "./src/app/components/login/technical/simple-dialog.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _components_extension_pin_code_pin_code_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./components/extension/pin-code/pin-code.component */ "./src/app/components/extension/pin-code/pin-code.component.ts");
/* harmony import */ var _components_login_menu_option_menu_option_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./components/login/menu-option/menu-option.component */ "./src/app/components/login/menu-option/menu-option.component.ts");
/* harmony import */ var _services_pin_code_data_pin_code_data_service__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./services/pin-code-data/pin-code-data.service */ "./src/app/services/pin-code-data/pin-code-data.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _components_menu_menu_component__WEBPACK_IMPORTED_MODULE_9__["MenuComponent"],
                _components_extension_extension_summary_extension_summary_component__WEBPACK_IMPORTED_MODULE_8__["ExtensionSummaryComponent"],
                _components_extension_extension_detail_extension_detail_component__WEBPACK_IMPORTED_MODULE_7__["ExtensionDetailComponent"],
                _components_top_calls_top_calls_top_calls_component__WEBPACK_IMPORTED_MODULE_12__["TopCallsComponent"],
                _components_login_technical_technical_component__WEBPACK_IMPORTED_MODULE_15__["TechnicalComponent"],
                _components_login_menu_menu_component__WEBPACK_IMPORTED_MODULE_16__["LoginMenuComponent"],
                _components_login_technical_simple_dialog_component__WEBPACK_IMPORTED_MODULE_18__["SimpleDialogComponent"],
                _components_extension_pin_code_pin_code_component__WEBPACK_IMPORTED_MODULE_20__["PinCodeComponent"],
                _components_login_menu_option_menu_option_component__WEBPACK_IMPORTED_MODULE_21__["MenuOptionComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatButtonModule"],
                _angular_material_table__WEBPACK_IMPORTED_MODULE_5__["MatTableModule"],
                _angular_material_input__WEBPACK_IMPORTED_MODULE_6__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSelectModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_11__["BrowserAnimationsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSortModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatGridListModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_19__["FormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCheckboxModule"]
            ],
            entryComponents: [
                _components_login_technical_simple_dialog_component__WEBPACK_IMPORTED_MODULE_18__["SimpleDialogComponent"]
            ],
            providers: [
                _services_extension_summary_data_extension_summary_data_service__WEBPACK_IMPORTED_MODULE_10__["ExtensionSummaryDataService"],
                _services_top_calls_data_top_calls_by_duration_service__WEBPACK_IMPORTED_MODULE_13__["TopCallsByDurationService"],
                _services_extension_detailed_data_extension_detailed_data_service__WEBPACK_IMPORTED_MODULE_14__["ExtensionDetailedDataService"],
                _services_login_technical_data_login_technical_data_service__WEBPACK_IMPORTED_MODULE_17__["LoginTechnicalDataService"],
                _services_pin_code_data_pin_code_data_service__WEBPACK_IMPORTED_MODULE_22__["PinCodeDataService"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/extension/extension-detail/extension-detail.component.css":
/*!**************************************************************************************!*\
  !*** ./src/app/components/extension/extension-detail/extension-detail.component.css ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-table {\r\n    width: 100%;\r\n}\r\n\r\n.mat-column {\r\n    width: 20%;\r\n    border-right: 1px solid gray;\r\n    align-self: stretch;\r\n    text-align: center;\r\n}\r\n\r\n/*.mat-column-numDialled {*/\r\n\r\n/*width: 20%;*/\r\n\r\n/*border-right: 1px solid gray;*/\r\n\r\n/*align-self: stretch;*/\r\n\r\n/*text-align: center;*/\r\n\r\n/*}*/\r\n\r\n/*.mat-column-region {*/\r\n\r\n/*width: 20%;*/\r\n\r\n/*border-right: 1px solid gray;*/\r\n\r\n/*align-self: stretch;*/\r\n\r\n/*text-align: center;*/\r\n\r\n/*}*/\r\n\r\n/*.mat-column-ringTime {*/\r\n\r\n/*width: 20%;*/\r\n\r\n/*border-right: 1px solid gray;*/\r\n\r\n/*align-self: stretch;*/\r\n\r\n/*text-align: center;*/\r\n\r\n/*}*/\r\n\r\n/*.mat-column-cost{*/\r\n\r\n/*width: 20%;*/\r\n\r\n/*border-right: 1px solid gray;*/\r\n\r\n/*align-self: stretch;*/\r\n\r\n/*text-align: center;*/\r\n\r\n/*}*/\r\n\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9leHRlbnNpb24vZXh0ZW5zaW9uLWRldGFpbC9leHRlbnNpb24tZGV0YWlsLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxZQUFZO0NBQ2Y7O0FBRUQ7SUFDSSxXQUFXO0lBQ1gsNkJBQTZCO0lBQzdCLG9CQUFvQjtJQUNwQixtQkFBbUI7Q0FDdEI7O0FBRUQsNEJBQTRCOztBQUN4QixlQUFlOztBQUNmLGlDQUFpQzs7QUFDakMsd0JBQXdCOztBQUN4Qix1QkFBdUI7O0FBQzNCLEtBQUs7O0FBRUwsd0JBQXdCOztBQUNwQixlQUFlOztBQUNmLGlDQUFpQzs7QUFDakMsd0JBQXdCOztBQUN4Qix1QkFBdUI7O0FBQzNCLEtBQUs7O0FBRUwsMEJBQTBCOztBQUN0QixlQUFlOztBQUNmLGlDQUFpQzs7QUFDakMsd0JBQXdCOztBQUN4Qix1QkFBdUI7O0FBQzNCLEtBQUs7O0FBRUwscUJBQXFCOztBQUNqQixlQUFlOztBQUNmLGlDQUFpQzs7QUFDakMsd0JBQXdCOztBQUN4Qix1QkFBdUI7O0FBQzNCLEtBQUsiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2V4dGVuc2lvbi9leHRlbnNpb24tZGV0YWlsL2V4dGVuc2lvbi1kZXRhaWwuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYXQtdGFibGUge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5tYXQtY29sdW1uIHtcclxuICAgIHdpZHRoOiAyMCU7XHJcbiAgICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCBncmF5O1xyXG4gICAgYWxpZ24tc2VsZjogc3RyZXRjaDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLyoubWF0LWNvbHVtbi1udW1EaWFsbGVkIHsqL1xyXG4gICAgLyp3aWR0aDogMjAlOyovXHJcbiAgICAvKmJvcmRlci1yaWdodDogMXB4IHNvbGlkIGdyYXk7Ki9cclxuICAgIC8qYWxpZ24tc2VsZjogc3RyZXRjaDsqL1xyXG4gICAgLyp0ZXh0LWFsaWduOiBjZW50ZXI7Ki9cclxuLyp9Ki9cclxuXHJcbi8qLm1hdC1jb2x1bW4tcmVnaW9uIHsqL1xyXG4gICAgLyp3aWR0aDogMjAlOyovXHJcbiAgICAvKmJvcmRlci1yaWdodDogMXB4IHNvbGlkIGdyYXk7Ki9cclxuICAgIC8qYWxpZ24tc2VsZjogc3RyZXRjaDsqL1xyXG4gICAgLyp0ZXh0LWFsaWduOiBjZW50ZXI7Ki9cclxuLyp9Ki9cclxuXHJcbi8qLm1hdC1jb2x1bW4tcmluZ1RpbWUgeyovXHJcbiAgICAvKndpZHRoOiAyMCU7Ki9cclxuICAgIC8qYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgZ3JheTsqL1xyXG4gICAgLyphbGlnbi1zZWxmOiBzdHJldGNoOyovXHJcbiAgICAvKnRleHQtYWxpZ246IGNlbnRlcjsqL1xyXG4vKn0qL1xyXG5cclxuLyoubWF0LWNvbHVtbi1jb3N0eyovXHJcbiAgICAvKndpZHRoOiAyMCU7Ki9cclxuICAgIC8qYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgZ3JheTsqL1xyXG4gICAgLyphbGlnbi1zZWxmOiBzdHJldGNoOyovXHJcbiAgICAvKnRleHQtYWxpZ246IGNlbnRlcjsqL1xyXG4vKn0qL1xyXG5cclxuXHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/components/extension/extension-detail/extension-detail.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/components/extension/extension-detail/extension-detail.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h4>Detailed Extension Report</h4>\r\n\r\n<form>\r\n    <mat-form-field>\r\n        <input matInput #srcToSearch placeholder=\"Extension Number\">\r\n    </mat-form-field>\r\n</form>\r\n\r\n<button mat-raised-button (click)=\"getBySrc(srcToSearch.value)\">Search</button>\r\n\r\n<mat-table [dataSource]=\"dataSource\" matSort>\r\n    <ng-container matColumnDef=\"callDate\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header> Date and Time </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.callDate}}</mat-cell>\r\n    </ng-container>\r\n\r\n    <ng-container matColumnDef=\"clid\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header> Clid </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.clid}}</mat-cell>\r\n    </ng-container>\r\n\r\n    <ng-container matColumnDef=\"src\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header> Source </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.src}}</mat-cell>\r\n    </ng-container>\r\n\r\n    <ng-container matColumnDef=\"destination\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header> Destination </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.destination}}</mat-cell>\r\n    </ng-container>\r\n\r\n    <ng-container matColumnDef=\"duration\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header> Duration </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.duration}}</mat-cell>\r\n    </ng-container>\r\n\r\n    <ng-container matColumnDef=\"cost\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header> Cost </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.cost}}</mat-cell>\r\n    </ng-container>\r\n\r\n\r\n    <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n    <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n</mat-table>\r\n"

/***/ }),

/***/ "./src/app/components/extension/extension-detail/extension-detail.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/components/extension/extension-detail/extension-detail.component.ts ***!
  \*************************************************************************************/
/*! exports provided: ExtensionDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExtensionDetailComponent", function() { return ExtensionDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_extension_detailed_data_extension_detailed_data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/extension-detailed-data/extension-detailed-data.service */ "./src/app/services/extension-detailed-data/extension-detailed-data.service.ts");
/* harmony import */ var _util_ExtensionDetailedDataSource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../util/ExtensionDetailedDataSource */ "./src/app/util/ExtensionDetailedDataSource.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ExtensionDetailComponent = /** @class */ (function () {
    function ExtensionDetailComponent(extensionDetailedDataService) {
        this.extensionDetailedDataService = extensionDetailedDataService;
        this.extensions = [];
        this.displayedColumns = ['callDate', 'clid', 'src', 'destination', 'duration', 'cost'];
        this.showTable = false;
    }
    ExtensionDetailComponent.prototype.ngOnInit = function () {
    };
    ExtensionDetailComponent.prototype.getBySrc = function (src) {
        var _this = this;
        this.extensionDetailedDataService.getBySrc(src, this.allowInternal).subscribe(function (extensions) {
            _this.extensions = extensions;
        });
        this.initDataSource();
    };
    ExtensionDetailComponent.prototype.initDataSource = function () {
        this.dataSource = new _util_ExtensionDetailedDataSource__WEBPACK_IMPORTED_MODULE_2__["ExtensionDetailedDataSource"](this.extensions, this.sort);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('allowInternal'),
        __metadata("design:type", Object)
    ], ExtensionDetailComponent.prototype, "allowInternal", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"])
    ], ExtensionDetailComponent.prototype, "sort", void 0);
    ExtensionDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-extension-detail',
            template: __webpack_require__(/*! ./extension-detail.component.html */ "./src/app/components/extension/extension-detail/extension-detail.component.html"),
            styles: [__webpack_require__(/*! ./extension-detail.component.css */ "./src/app/components/extension/extension-detail/extension-detail.component.css")]
        }),
        __metadata("design:paramtypes", [_services_extension_detailed_data_extension_detailed_data_service__WEBPACK_IMPORTED_MODULE_1__["ExtensionDetailedDataService"]])
    ], ExtensionDetailComponent);
    return ExtensionDetailComponent;
}());



/***/ }),

/***/ "./src/app/components/extension/extension-summary/extension-summary.component.css":
/*!****************************************************************************************!*\
  !*** ./src/app/components/extension/extension-summary/extension-summary.component.css ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-table {\r\n    width: 100%;\r\n}\r\n\r\n/*.mat-column-ext {*/\r\n\r\n/*width: 16.66%;*/\r\n\r\n/*border-right: 1px solid gray;*/\r\n\r\n/*align-self: stretch;*/\r\n\r\n/*text-align: center;*/\r\n\r\n/*}*/\r\n\r\n/*.mat-column-extName {*/\r\n\r\n/*width: 16.66%;*/\r\n\r\n/*border-right: 1px solid gray;*/\r\n\r\n/*align-self: stretch;*/\r\n\r\n/*text-align: center;*/\r\n\r\n/*}*/\r\n\r\n/*.mat-column-local {*/\r\n\r\n/*width: 16.66%;*/\r\n\r\n/*border-right: 1px solid gray;*/\r\n\r\n/*align-self: stretch;*/\r\n\r\n/*text-align: center;*/\r\n\r\n/*}*/\r\n\r\n/*.mat-column-mobile {*/\r\n\r\n/*width: 16.66%;*/\r\n\r\n/*border-right: 1px solid gray;*/\r\n\r\n/*align-self: stretch;*/\r\n\r\n/*text-align: center;*/\r\n\r\n/*}*/\r\n\r\n/*.mat-column-int{*/\r\n\r\n/*width: 16.66%;*/\r\n\r\n/*border-right: 1px solid gray;*/\r\n\r\n/*align-self: stretch;*/\r\n\r\n/*text-align: center;*/\r\n\r\n/*}*/\r\n\r\n/*.mat-column-total{*/\r\n\r\n/*width: 16.66%;*/\r\n\r\n/*align-self: stretch;*/\r\n\r\n/*text-align: center;*/\r\n\r\n/*}*/\r\n\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9leHRlbnNpb24vZXh0ZW5zaW9uLXN1bW1hcnkvZXh0ZW5zaW9uLXN1bW1hcnkuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLFlBQVk7Q0FDZjs7QUFFRCxxQkFBcUI7O0FBQ2pCLGtCQUFrQjs7QUFDbEIsaUNBQWlDOztBQUNqQyx3QkFBd0I7O0FBQ3hCLHVCQUF1Qjs7QUFDM0IsS0FBSzs7QUFFTCx5QkFBeUI7O0FBQ3JCLGtCQUFrQjs7QUFDbEIsaUNBQWlDOztBQUNqQyx3QkFBd0I7O0FBQ3hCLHVCQUF1Qjs7QUFDM0IsS0FBSzs7QUFFTCx1QkFBdUI7O0FBQ25CLGtCQUFrQjs7QUFDbEIsaUNBQWlDOztBQUNqQyx3QkFBd0I7O0FBQ3hCLHVCQUF1Qjs7QUFDM0IsS0FBSzs7QUFFTCx3QkFBd0I7O0FBQ3BCLGtCQUFrQjs7QUFDbEIsaUNBQWlDOztBQUNqQyx3QkFBd0I7O0FBQ3hCLHVCQUF1Qjs7QUFDM0IsS0FBSzs7QUFFTCxvQkFBb0I7O0FBQ2hCLGtCQUFrQjs7QUFDbEIsaUNBQWlDOztBQUNqQyx3QkFBd0I7O0FBQ3hCLHVCQUF1Qjs7QUFDM0IsS0FBSzs7QUFFTCxzQkFBc0I7O0FBQ2xCLGtCQUFrQjs7QUFDbEIsd0JBQXdCOztBQUN4Qix1QkFBdUI7O0FBQzNCLEtBQUsiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2V4dGVuc2lvbi9leHRlbnNpb24tc3VtbWFyeS9leHRlbnNpb24tc3VtbWFyeS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1hdC10YWJsZSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLyoubWF0LWNvbHVtbi1leHQgeyovXHJcbiAgICAvKndpZHRoOiAxNi42NiU7Ki9cclxuICAgIC8qYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgZ3JheTsqL1xyXG4gICAgLyphbGlnbi1zZWxmOiBzdHJldGNoOyovXHJcbiAgICAvKnRleHQtYWxpZ246IGNlbnRlcjsqL1xyXG4vKn0qL1xyXG5cclxuLyoubWF0LWNvbHVtbi1leHROYW1lIHsqL1xyXG4gICAgLyp3aWR0aDogMTYuNjYlOyovXHJcbiAgICAvKmJvcmRlci1yaWdodDogMXB4IHNvbGlkIGdyYXk7Ki9cclxuICAgIC8qYWxpZ24tc2VsZjogc3RyZXRjaDsqL1xyXG4gICAgLyp0ZXh0LWFsaWduOiBjZW50ZXI7Ki9cclxuLyp9Ki9cclxuXHJcbi8qLm1hdC1jb2x1bW4tbG9jYWwgeyovXHJcbiAgICAvKndpZHRoOiAxNi42NiU7Ki9cclxuICAgIC8qYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgZ3JheTsqL1xyXG4gICAgLyphbGlnbi1zZWxmOiBzdHJldGNoOyovXHJcbiAgICAvKnRleHQtYWxpZ246IGNlbnRlcjsqL1xyXG4vKn0qL1xyXG5cclxuLyoubWF0LWNvbHVtbi1tb2JpbGUgeyovXHJcbiAgICAvKndpZHRoOiAxNi42NiU7Ki9cclxuICAgIC8qYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgZ3JheTsqL1xyXG4gICAgLyphbGlnbi1zZWxmOiBzdHJldGNoOyovXHJcbiAgICAvKnRleHQtYWxpZ246IGNlbnRlcjsqL1xyXG4vKn0qL1xyXG5cclxuLyoubWF0LWNvbHVtbi1pbnR7Ki9cclxuICAgIC8qd2lkdGg6IDE2LjY2JTsqL1xyXG4gICAgLypib3JkZXItcmlnaHQ6IDFweCBzb2xpZCBncmF5OyovXHJcbiAgICAvKmFsaWduLXNlbGY6IHN0cmV0Y2g7Ki9cclxuICAgIC8qdGV4dC1hbGlnbjogY2VudGVyOyovXHJcbi8qfSovXHJcblxyXG4vKi5tYXQtY29sdW1uLXRvdGFseyovXHJcbiAgICAvKndpZHRoOiAxNi42NiU7Ki9cclxuICAgIC8qYWxpZ24tc2VsZjogc3RyZXRjaDsqL1xyXG4gICAgLyp0ZXh0LWFsaWduOiBjZW50ZXI7Ki9cclxuLyp9Ki9cclxuXHJcblxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/components/extension/extension-summary/extension-summary.component.html":
/*!*****************************************************************************************!*\
  !*** ./src/app/components/extension/extension-summary/extension-summary.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h4>Summary Extension Report</h4>\r\n\r\n<button mat-raised-button (click)=\"this.initDataSource()\">Generate report</button>\r\n\r\n<mat-table [dataSource]=\"dataSource\" matSort>\r\n\r\n    <ng-container matColumnDef=\"clid\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header> Extension Name</mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.clid}} </mat-cell>\r\n    </ng-container>\r\n\r\n    <ng-container matColumnDef=\"src\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header> Source </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.src}} </mat-cell>\r\n    </ng-container>\r\n\r\n    <!--<ng-container matColumnDef=\"dst\">-->\r\n        <!--<mat-header-cell *matHeaderCellDef mat-sort-header> Destination </mat-header-cell>-->\r\n        <!--<mat-cell *matCellDef=\"let element\"> {{element.destination}} </mat-cell>-->\r\n    <!--</ng-container>-->\r\n\r\n    <ng-container matColumnDef=\"duration\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header> Total Duration </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.duration}} </mat-cell>\r\n    </ng-container>\r\n\r\n    <ng-container matColumnDef=\"cost\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header> Cost </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.cost}} </mat-cell>\r\n    </ng-container>\r\n\r\n    <ng-container matColumnDef=\"numOfCalls\">\r\n        <mat-header-cell *matHeaderCellDef mat-sort-header> # of calls </mat-header-cell>\r\n        <mat-cell *matCellDef=\"let element\"> {{element.numOfCalls}} </mat-cell>\r\n    </ng-container>\r\n\r\n    <!--<ng-container matColumnDef=\"total\">-->\r\n        <!--<th mat-header-cell *matHeaderCellDef> Total </th>-->\r\n        <!--<td mat-cell *matCellDef=\"let element\"> {{element.total}} </td>-->\r\n    <!--</ng-container>-->\r\n\r\n    <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n    <mat-row *matRowDef=\"let element; columns: displayedColumns;\"></mat-row>\r\n</mat-table>"

/***/ }),

/***/ "./src/app/components/extension/extension-summary/extension-summary.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/components/extension/extension-summary/extension-summary.component.ts ***!
  \***************************************************************************************/
/*! exports provided: ExtensionSummaryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExtensionSummaryComponent", function() { return ExtensionSummaryComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_extension_summary_data_extension_summary_data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/extension-summary-data/extension-summary-data.service */ "./src/app/services/extension-summary-data/extension-summary-data.service.ts");
/* harmony import */ var _util_ExtensionSummaryDataSource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../util/ExtensionSummaryDataSource */ "./src/app/util/ExtensionSummaryDataSource.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ExtensionSummaryComponent = /** @class */ (function () {
    function ExtensionSummaryComponent(extensionSummaryDataService) {
        this.extensionSummaryDataService = extensionSummaryDataService;
        this.extensions = [];
        this.displayedColumns = ['clid', 'src', 'duration', 'cost', 'numOfCalls'];
    }
    ExtensionSummaryComponent.prototype.ngOnInit = function () {
        this.initExtensions();
    };
    ExtensionSummaryComponent.prototype.initExtensions = function () {
        var _this = this;
        this.extensionSummaryDataService.getAllExtensions().subscribe(function (extensions) {
            _this.extensions = extensions;
        });
    };
    ExtensionSummaryComponent.prototype.initDataSource = function () {
        this.dataSource = new _util_ExtensionSummaryDataSource__WEBPACK_IMPORTED_MODULE_2__["ExtensionSummaryDataSource"](this.extensions, this.sort);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('allowInternal'),
        __metadata("design:type", Object)
    ], ExtensionSummaryComponent.prototype, "allowInternal", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"])
    ], ExtensionSummaryComponent.prototype, "sort", void 0);
    ExtensionSummaryComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-extension-summary',
            template: __webpack_require__(/*! ./extension-summary.component.html */ "./src/app/components/extension/extension-summary/extension-summary.component.html"),
            styles: [__webpack_require__(/*! ./extension-summary.component.css */ "./src/app/components/extension/extension-summary/extension-summary.component.css")]
        }),
        __metadata("design:paramtypes", [_services_extension_summary_data_extension_summary_data_service__WEBPACK_IMPORTED_MODULE_1__["ExtensionSummaryDataService"]])
    ], ExtensionSummaryComponent);
    return ExtensionSummaryComponent;
}());



/***/ }),

/***/ "./src/app/components/extension/pin-code/pin-code.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/components/extension/pin-code/pin-code.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZXh0ZW5zaW9uL3Bpbi1jb2RlL3Bpbi1jb2RlLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/extension/pin-code/pin-code.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/components/extension/pin-code/pin-code.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h4>Pin Code Report</h4>\n\n<form>\n  <mat-form-field>\n    <input matInput #codeToSearch placeholder=\"Pin Code\">\n  </mat-form-field>\n</form>\n\n<button mat-raised-button (click)=\"getByPinCode(codeToSearch.value)\">Generate Report</button>\n\n<mat-table [dataSource]=\"dataSource\" matSort>\n\n  <ng-container matColumnDef=\"clid\">\n    <mat-header-cell *matHeaderCellDef mat-sort-header> Clid </mat-header-cell>\n    <mat-cell *matCellDef=\"let element\"> {{element.clid}} </mat-cell>\n  </ng-container>\n\n  <ng-container matColumnDef=\"src\">\n    <mat-header-cell *matHeaderCellDef mat-sort-header> Src </mat-header-cell>\n    <mat-cell *matCellDef=\"let element\"> {{element.src}} </mat-cell>\n  </ng-container>\n\n  <ng-container matColumnDef=\"destination\">\n    <mat-header-cell *matHeaderCellDef mat-sort-header> Destination </mat-header-cell>\n    <mat-cell *matCellDef=\"let element\"> {{element.destination}} </mat-cell>\n  </ng-container>\n\n  <ng-container matColumnDef=\"duration\">\n    <mat-header-cell *matHeaderCellDef mat-sort-header> Duration </mat-header-cell>\n    <mat-cell *matCellDef=\"let element\"> {{element.duration}} </mat-cell>\n  </ng-container>\n\n  <ng-container matColumnDef=\"cost\">\n    <mat-header-cell *matHeaderCellDef mat-sort-header> Cost </mat-header-cell>\n    <mat-cell *matCellDef=\"let element\"> {{element.cost}} </mat-cell>\n  </ng-container>\n\n\n\n  <!--<ng-container matColumnDef=\"id\">-->\n  <!--<mat-header-cell *matHeaderCellDef> International </mat-header-cell>-->\n  <!--<mat-cell *matCellDef=\"let element\"> {{element.id}} </mat-cell>-->\n  <!--</ng-container>-->\n\n  <!--<ng-container matColumnDef=\"total\">-->\n  <!--<th mat-header-cell *matHeaderCellDef> Total </th>-->\n  <!--<td mat-cell *matCellDef=\"let element\"> {{element.total}} </td>-->\n  <!--</ng-container>-->\n\n  <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\n  <mat-row *matRowDef=\"let element; columns: displayedColumns;\"></mat-row>\n</mat-table>"

/***/ }),

/***/ "./src/app/components/extension/pin-code/pin-code.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/components/extension/pin-code/pin-code.component.ts ***!
  \*********************************************************************/
/*! exports provided: PinCodeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PinCodeComponent", function() { return PinCodeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _util_PinCodeDataSource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../util/PinCodeDataSource */ "./src/app/util/PinCodeDataSource.ts");
/* harmony import */ var _services_pin_code_data_pin_code_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/pin-code-data/pin-code-data.service */ "./src/app/services/pin-code-data/pin-code-data.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PinCodeComponent = /** @class */ (function () {
    function PinCodeComponent(pinCodeDataService) {
        this.pinCodeDataService = pinCodeDataService;
        this.extensions = [];
        this.displayedColumns = ['clid', 'src', 'destination', 'duration', 'cost'];
    }
    PinCodeComponent.prototype.ngOnInit = function () {
    };
    PinCodeComponent.prototype.getByPinCode = function (pinCode) {
        var _this = this;
        this.pinCodeDataService.getByPinCode(pinCode, this.allowInternal).subscribe(function (extensions) {
            _this.extensions = extensions;
        });
        this.initDataSource();
    };
    PinCodeComponent.prototype.initDataSource = function () {
        this.dataSource = new _util_PinCodeDataSource__WEBPACK_IMPORTED_MODULE_2__["PinCodeDataSource"](this.extensions, this.sort);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('allowInternal'),
        __metadata("design:type", Object)
    ], PinCodeComponent.prototype, "allowInternal", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], PinCodeComponent.prototype, "sort", void 0);
    PinCodeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-pin-code',
            template: __webpack_require__(/*! ./pin-code.component.html */ "./src/app/components/extension/pin-code/pin-code.component.html"),
            styles: [__webpack_require__(/*! ./pin-code.component.css */ "./src/app/components/extension/pin-code/pin-code.component.css")]
        }),
        __metadata("design:paramtypes", [_services_pin_code_data_pin_code_data_service__WEBPACK_IMPORTED_MODULE_3__["PinCodeDataService"]])
    ], PinCodeComponent);
    return PinCodeComponent;
}());



/***/ }),

/***/ "./src/app/components/login/menu-option/menu-option.component.css":
/*!************************************************************************!*\
  !*** ./src/app/components/login/menu-option/menu-option.component.css ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbG9naW4vbWVudS1vcHRpb24vbWVudS1vcHRpb24uY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/components/login/menu-option/menu-option.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/components/login/menu-option/menu-option.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"option === ''\">\n  <mat-card style=\"width: 20%\">\n    <p style=\"margin-left: 17%\">What would you like to do?</p>\n    <div style=\"width: 63%; margin-left: 17%\">\n      <button mat-raised-button (click)=\"option = 'setup'\">Setup</button>\n      <button mat-raised-button (click)=\"option = 'report'\">Reports</button>\n    </div>\n  </mat-card>\n</div>\n\n<app-technical *ngIf=\"option === 'setup'\"></app-technical>\n<app-menu *ngIf=\"option === 'report'\"></app-menu>\n\n"

/***/ }),

/***/ "./src/app/components/login/menu-option/menu-option.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/components/login/menu-option/menu-option.component.ts ***!
  \***********************************************************************/
/*! exports provided: MenuOptionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuOptionComponent", function() { return MenuOptionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MenuOptionComponent = /** @class */ (function () {
    function MenuOptionComponent() {
        this.option = '';
    }
    MenuOptionComponent.prototype.ngOnInit = function () {
    };
    MenuOptionComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-menu-option',
            template: __webpack_require__(/*! ./menu-option.component.html */ "./src/app/components/login/menu-option/menu-option.component.html"),
            styles: [__webpack_require__(/*! ./menu-option.component.css */ "./src/app/components/login/menu-option/menu-option.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], MenuOptionComponent);
    return MenuOptionComponent;
}());



/***/ }),

/***/ "./src/app/components/login/menu/menu.component.css":
/*!**********************************************************!*\
  !*** ./src/app/components/login/menu/menu.component.css ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbG9naW4vbWVudS9tZW51LmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/login/menu/menu.component.html":
/*!***********************************************************!*\
  !*** ./src/app/components/login/menu/menu.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"this.credentialsOk === false\">\n  <h2> Select login type </h2>\n\n  <mat-form-field>\n    <mat-select #loginOption placeholder=\"Select type:\">\n      <mat-option value=\"technical\" (click)=\"setLoginOption('technical')\">Technical (Setup)</mat-option>\n      <mat-option value=\"general\" (click)=\"setLoginOption('general')\">General (Reports)</mat-option>\n      <!--<mat-option value=\"management\">Management</mat-option>-->\n      <!--<mat-option value=\"operator\">Operator</mat-option>-->\n    </mat-select>\n  </mat-form-field>\n\n  <br>\n\n  <form>\n    <mat-form-field>\n      <input matInput #username autocomplete=\"off\" placeholder=\"Username\">\n    </mat-form-field>\n\n    <br>\n\n    <mat-form-field>\n      <input matInput #password [type]=\"this.hidePassword ? 'password' : 'text'\" autocomplete=\"off\" placeholder=\"Password\">\n    </mat-form-field>\n\n    <button mat-icon-button (mousedown)=\"this.hidePassword = !this.hidePassword;\" (mouseup)=\"this.hidePassword = !this.hidePassword;\">\n      <mat-icon>visibility</mat-icon>\n    </button>\n  </form>\n\n  <button mat-raised-button\n          [disabled]=\"(loginOption.value === undefined) || (username.value === '') || (password.value === '')\"\n          (click)=\"checkCredentials(username.value, password.value, loginOption.value); this.hideSetupPage = false;\">Login</button>\n\n  <br>\n\n  <p *ngIf=\"this.inputError === true\" style=\"color: red\">Invalid input. Please try again.</p>\n\n</div>\n<app-technical\n        *ngIf=\"this.credentialsOk === true && this.loginOption === 'technical'\"\n        (hideSetupPageEvent)=\"resetLoginPage($event)\"></app-technical>\n<app-menu *ngIf=\"this.credentialsOk === true && this.loginOption === 'general'\"></app-menu>\n\n"

/***/ }),

/***/ "./src/app/components/login/menu/menu.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/components/login/menu/menu.component.ts ***!
  \*********************************************************/
/*! exports provided: LoginMenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginMenuComponent", function() { return LoginMenuComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LoginMenuComponent = /** @class */ (function () {
    function LoginMenuComponent() {
        this.technicalUsers = [
            { username: 'teeray', password: '7473' }
        ];
        this.generalUsers = [
            { username: 'test', password: '123' }
        ];
        this.credentialsOk = false;
        this.loginOption = null;
        this.inputError = false;
        this.hideSetupPage = true;
        this.hidePassword = true;
    }
    LoginMenuComponent.prototype.ngOnInit = function () {
    };
    LoginMenuComponent.prototype.setLoginOption = function (loginOption) {
        this.loginOption = loginOption;
        console.log(this.loginOption);
    };
    LoginMenuComponent.prototype.checkCredentials = function (username, password, loginOption) {
        var _this = this;
        console.log(loginOption);
        if (loginOption === 'technical') {
            this.technicalUsers.forEach(function (user) {
                if (user.username === username && user.password === password) {
                    _this.credentialsOk = true;
                    _this.inputError = false;
                }
            });
        }
        else {
            this.generalUsers.forEach(function (user) {
                if (user.username === username && user.password === password) {
                    _this.credentialsOk = true;
                    _this.inputError = false;
                }
            });
        }
        if (this.credentialsOk === false) {
            this.inputError = true;
        }
    };
    LoginMenuComponent.prototype.resetLoginPage = function (hideSetupPage) {
        this.credentialsOk = false;
        this.hideSetupPage = hideSetupPage;
    };
    LoginMenuComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login-menu',
            template: __webpack_require__(/*! ./menu.component.html */ "./src/app/components/login/menu/menu.component.html"),
            styles: [__webpack_require__(/*! ./menu.component.css */ "./src/app/components/login/menu/menu.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], LoginMenuComponent);
    return LoginMenuComponent;
}());



/***/ }),

/***/ "./src/app/components/login/technical/simple-dialog.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/components/login/technical/simple-dialog.component.ts ***!
  \***********************************************************************/
/*! exports provided: SimpleDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SimpleDialogComponent", function() { return SimpleDialogComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var SimpleDialogComponent = /** @class */ (function () {
    function SimpleDialogComponent() {
    }
    SimpleDialogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            template: "\n        <h3>Database created successfully</h3>\n    "
        })
    ], SimpleDialogComponent);
    return SimpleDialogComponent;
}());



/***/ }),

/***/ "./src/app/components/login/technical/technical.component.css":
/*!********************************************************************!*\
  !*** ./src/app/components/login/technical/technical.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbG9naW4vdGVjaG5pY2FsL3RlY2huaWNhbC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/login/technical/technical.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/components/login/technical/technical.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div style=\"width: 100%;\" *ngIf=\"showReport === false\">\n\n    <mat-card style=\"font-size: 24px\">Technical Login Page</mat-card>\n\n    <div style=\"margin-top: 20px; float: left; width: 500px\">\n        <!--<input #chooseFile type=\"file\" (change)=\"uploadFile($event); showSpinner = true\" accept=\".csv\">-->\n\n        <!--<button mat-raised-button >Upload file</button>-->\n\n        <!--<mat-spinner *ngIf=\"showSpinner === true\"></mat-spinner>-->\n\n        <!--<br>-->\n\n        <p>Database Connection Details</p>\n\n        <mat-form-field style=\"margin-right: 5px\">\n            <input matInput #username placeholder=\"Username\">\n        </mat-form-field>\n\n        <mat-form-field>\n            <input matInput #password placeholder=\"Password\">\n        </mat-form-field>\n\n        <br>\n\n        <!--<mat-form-field style=\"margin-right: 5px\">-->\n            <!--<input #name matInput placeholder=\"Name\" [(ngModel)]=\"inputName\">-->\n        <!--</mat-form-field>-->\n\n        <!--<mat-form-field>-->\n            <!--<input #pinCode matInput placeholder=\"Pin code\" [(ngModel)]=\"inputPinCode\">-->\n        <!--</mat-form-field>-->\n\n        <!--<button mat-raised-button [disabled]=\"((inputName === null || inputName === '') || (inputPinCode === null || inputPinCode === ''))\" (click)=\"addToList(name.value, pinCode.value)\" style=\"margin-left: 20px\">Add</button>-->\n\n        <!--<br>-->\n\n        <button mat-raised-button (click)=\"uploadDetails(username.value, password.value); hideSetupPageFunction(true)\">Confirm</button>\n        <p style=\"color: red; font-size: 12\">Note: Clicking 'Confirm' will log you out. You must login again as a 'General' user to view reports.</p>\n\n        <br>\n\n        <button mat-raised-button (click)=\"hideSetupPageFunction(true)\">Cancel and return to login</button>\n    </div>\n\n    <!--<mat-list *ngIf=\"names.length > 1\" style=\"width: 200px; float: left\">-->\n        <!--<mat-list-item *ngFor=\"let name of names\">{{name}}</mat-list-item>-->\n    <!--</mat-list>-->\n\n    <!--<mat-grid-list cols=\"2\" rowHeight=\"30px\" style=\"width: 200px; float: left; margin-top: 20px\">-->\n        <!--<mat-grid-tile-->\n                <!--*ngFor=\"let name of names\"-->\n                <!--[colspan]=\"name.cols\"-->\n                <!--[rowspan]=\"name.rows\">-->\n            <!--{{name.text}}-->\n        <!--</mat-grid-tile>-->\n    <!--</mat-grid-list>-->\n\n</div>\n\n<!--<app-menu *ngIf=\"showReport === true\"></app-menu>-->\n<app-login-menu *ngIf=\"this.hideSetupPage === true\"></app-login-menu>"

/***/ }),

/***/ "./src/app/components/login/technical/technical.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/components/login/technical/technical.component.ts ***!
  \*******************************************************************/
/*! exports provided: TechnicalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TechnicalComponent", function() { return TechnicalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_login_technical_data_login_technical_data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/login-technical-data/login-technical-data.service */ "./src/app/services/login-technical-data/login-technical-data.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _models_DbUser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../models/DbUser */ "./src/app/models/DbUser.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TechnicalComponent = /** @class */ (function () {
    function TechnicalComponent(loginTechnicalService, dialog) {
        this.loginTechnicalService = loginTechnicalService;
        this.dialog = dialog;
        this.showReport = false;
        this.hideSetupPage = false;
        this.hideSetupPageEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        // STAGE 2
        // showSpinner = false;
        // isComplete = false;
        //
        // dialogRef: MatDialogRef<SimpleDialogComponent>;
        // inputName = null;
        // inputPinCode = null;
        // names: Tile[] = [
        //     {text: 'Name', cols: 1, rows: 1},
        //     {text: 'Pin Code', cols: 1, rows: 1}
        // ];
        //
        this.nameAndPins = [];
    }
    TechnicalComponent.prototype.ngOnInit = function () {
    };
    // STAGE 2
    // async uploadFile(event) {
    //     const file: File = event.target.files[0];
    //
    //     if (file !== null) {
    //         const formData: FormData = new FormData();
    //
    //         formData.append('file', file, file.name);
    //
    //         await this.loginTechnicalService.uploadFileTest(formData)
    //             .then(
    //                 res => this.isComplete = res
    //             );
    //
    //         console.log(this.isComplete);
    //
    //         if (this.isComplete === true) {
    //             this.showSpinner = false;
    //             this.dialogRef = this.dialog.open(SimpleDialogComponent);
    //         }
    //
    //     }
    // }
    // STAGE 2
    // addToList(name, pinCode) {
    //     const nameAndPin = new NameAndPin();
    //
    //     nameAndPin.name = name;
    //     nameAndPin.pinCode = pinCode;
    //
    //     this.nameAndPins.push(nameAndPin);
    //
    //     this.names.push({text: name, cols: 1, rows: 1});
    //     this.names.push({text: pinCode, cols: 1, rows: 1});
    // }
    TechnicalComponent.prototype.uploadDetails = function (username, password) {
        console.log(password);
        var dbUser = new _models_DbUser__WEBPACK_IMPORTED_MODULE_3__["DbUser"]();
        dbUser.username = username;
        dbUser.password = password;
        console.log(dbUser.username);
        this.loginTechnicalService.uploadDetails(dbUser.toJSON(), JSON.stringify(this.nameAndPins));
    };
    TechnicalComponent.prototype.hideSetupPageFunction = function () {
        this.hideSetupPage = true;
        this.hideSetupPageEvent.next(this.hideSetupPage);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], TechnicalComponent.prototype, "hideSetupPageEvent", void 0);
    TechnicalComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-technical',
            template: __webpack_require__(/*! ./technical.component.html */ "./src/app/components/login/technical/technical.component.html"),
            styles: [__webpack_require__(/*! ./technical.component.css */ "./src/app/components/login/technical/technical.component.css")]
        }),
        __metadata("design:paramtypes", [_services_login_technical_data_login_technical_data_service__WEBPACK_IMPORTED_MODULE_1__["LoginTechnicalDataService"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"]])
    ], TechnicalComponent);
    return TechnicalComponent;
}());



/***/ }),

/***/ "./src/app/components/menu/menu.component.css":
/*!****************************************************!*\
  !*** ./src/app/components/menu/menu.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbWVudS9tZW51LmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/menu/menu.component.html":
/*!*****************************************************!*\
  !*** ./src/app/components/menu/menu.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-card>\r\n    <mat-card-content>\r\n        <h2>Reporting Tool</h2>\r\n        <mat-tab-group>\r\n            <mat-tab label=\"Extension\">\r\n                <br/>\r\n                <mat-form-field>\r\n                    <mat-select #select placeholder=\"Report Type:\">\r\n                        <mat-option value=\"summary\">Summary</mat-option>\r\n                        <mat-option value=\"detailed\">Detailed</mat-option>\r\n                        <mat-option value=\"pinCode\">Pin Code</mat-option>\r\n                    </mat-select>\r\n                </mat-form-field>\r\n                <br/>\r\n                <section class=\"example-section\">\r\n                    <mat-checkbox class=\"example-margin\" [(ngModel)]=\"allowInternal\">Display internal calls</mat-checkbox>\r\n                </section>\r\n                <br>\r\n                <!--<button mat-raised-button (click)=\"view = select.value\">Go</button>-->\r\n                <app-extension-summary [allowInternal]=\"this.allowInternal\" *ngIf=\"select.value === 'summary'\"></app-extension-summary>\r\n                <app-extension-detail [allowInternal]=\"this.allowInternal\" *ngIf=\"select.value === 'detailed'\"></app-extension-detail>\r\n                <app-pin-code [allowInternal]=\"this.allowInternal\" *ngIf=\"select.value === 'pinCode'\"></app-pin-code>\r\n            </mat-tab>\r\n            <mat-tab label=\"Top Calls\">\r\n                <br/>\r\n                <mat-form-field>\r\n                    <mat-select #howManyCalls placeholder=\"Top:\">\r\n                        <mat-option value=\"10\">10</mat-option>\r\n                        <mat-option value=\"20\">20</mat-option>\r\n                        <mat-option value=\"40\">40</mat-option>\r\n                    </mat-select>\r\n                </mat-form-field>\r\n                <br/>\r\n                <mat-form-field>\r\n                    <mat-select #sortBy placeholder=\"Sort by:\">\r\n                        <!--<mat-option value=\"cost\">Cost</mat-option>-->\r\n                        <mat-option value=\"duration\">Duration</mat-option>\r\n                        <!--<mat-option value=\"numberDialled\">Number Dialled</mat-option>-->\r\n                    </mat-select>\r\n                </mat-form-field>\r\n                <br/>\r\n                <button mat-raised-button (click)=\"getTopCallsByDuration(howManyCalls.value)\">Proceed</button>\r\n                <app-top-calls></app-top-calls>\r\n            </mat-tab>\r\n        </mat-tab-group>\r\n    </mat-card-content>\r\n</mat-card>\r\n\r\n\r\n<!--<br/>-->\r\n\r\n<!--<button mat-raised-button (click)=\"extSummary = true;\">Extension Summary</button>-->\r\n<!--<app-extension-summary *ngIf=\"extSummary\"></app-extension-summary>-->\r\n\r\n\r\n<!--<br/>-->\r\n\r\n<!--<button mat-raised-button (click)=\"extDetail = true;\">Extension Detail</button>-->\r\n<!--<app-extension-detail *ngIf=\"extDetail\"></app-extension-detail>-->"

/***/ }),

/***/ "./src/app/components/menu/menu.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/menu/menu.component.ts ***!
  \***************************************************/
/*! exports provided: MenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuComponent", function() { return MenuComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _top_calls_top_calls_top_calls_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../top-calls/top-calls/top-calls.component */ "./src/app/components/top-calls/top-calls/top-calls.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MenuComponent = /** @class */ (function () {
    function MenuComponent() {
        this.allowInternal = false;
    }
    MenuComponent.prototype.ngOnInit = function () {
    };
    MenuComponent.prototype.getTopCallsByDuration = function (howMany) {
        this.topCalls.getTopCallsByDuration(howMany);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_top_calls_top_calls_top_calls_component__WEBPACK_IMPORTED_MODULE_1__["TopCallsComponent"]),
        __metadata("design:type", _top_calls_top_calls_top_calls_component__WEBPACK_IMPORTED_MODULE_1__["TopCallsComponent"])
    ], MenuComponent.prototype, "topCalls", void 0);
    MenuComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-menu',
            template: __webpack_require__(/*! ./menu.component.html */ "./src/app/components/menu/menu.component.html"),
            styles: [__webpack_require__(/*! ./menu.component.css */ "./src/app/components/menu/menu.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], MenuComponent);
    return MenuComponent;
}());



/***/ }),

/***/ "./src/app/components/top-calls/top-calls/top-calls.component.css":
/*!************************************************************************!*\
  !*** ./src/app/components/top-calls/top-calls/top-calls.component.css ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdG9wLWNhbGxzL3RvcC1jYWxscy90b3AtY2FsbHMuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/components/top-calls/top-calls/top-calls.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/components/top-calls/top-calls/top-calls.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h4>Extension Summary</h4>\r\n\r\n<mat-table [dataSource]=\"dataSource\" matSort>\r\n  <ng-container matColumnDef=\"callDate\">\r\n    <mat-header-cell *matHeaderCellDef mat-sort-header> Date and Time </mat-header-cell>\r\n    <mat-cell *matCellDef=\"let element\"> {{element.callDate}} </mat-cell>\r\n  </ng-container>\r\n\r\n  <ng-container matColumnDef=\"clid\">\r\n    <mat-header-cell *matHeaderCellDef mat-sort-header> Clid </mat-header-cell>\r\n    <mat-cell *matCellDef=\"let element\"> {{element.clid}} </mat-cell>\r\n  </ng-container>\r\n\r\n  <ng-container matColumnDef=\"src\">\r\n    <mat-header-cell *matHeaderCellDef mat-sort-header> Source </mat-header-cell>\r\n    <mat-cell *matCellDef=\"let element\"> {{element.src}} </mat-cell>\r\n  </ng-container>\r\n\r\n  <ng-container matColumnDef=\"destination\">\r\n    <mat-header-cell *matHeaderCellDef mat-sort-header> Destination </mat-header-cell>\r\n    <mat-cell *matCellDef=\"let element\"> {{element.destination}} </mat-cell>\r\n  </ng-container>\r\n\r\n  <ng-container matColumnDef=\"duration\">\r\n    <mat-header-cell *matHeaderCellDef mat-sort-header> Duration </mat-header-cell>\r\n    <mat-cell *matCellDef=\"let element\"> {{element.duration}} </mat-cell>\r\n  </ng-container>\r\n\r\n  <ng-container matColumnDef=\"cost\">\r\n    <mat-header-cell *matHeaderCellDef mat-sort-header> Cost </mat-header-cell>\r\n    <mat-cell *matCellDef=\"let element\"> {{element.cost}} </mat-cell>\r\n  </ng-container>\r\n\r\n  <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n  <mat-row *matRowDef=\"let element; columns: displayedColumns;\"></mat-row>\r\n</mat-table>"

/***/ }),

/***/ "./src/app/components/top-calls/top-calls/top-calls.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/components/top-calls/top-calls/top-calls.component.ts ***!
  \***********************************************************************/
/*! exports provided: TopCallsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TopCallsComponent", function() { return TopCallsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _util_TopCallsDataSource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../util/TopCallsDataSource */ "./src/app/util/TopCallsDataSource.ts");
/* harmony import */ var _services_top_calls_data_top_calls_by_duration_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/top-calls-data/top-calls-by-duration.service */ "./src/app/services/top-calls-data/top-calls-by-duration.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TopCallsComponent = /** @class */ (function () {
    function TopCallsComponent(topCallsByDurationService) {
        this.topCallsByDurationService = topCallsByDurationService;
        this.extensions = [];
        this.displayedColumns = ['callDate', 'clid', 'src', 'destination', 'duration', 'cost'];
    }
    TopCallsComponent.prototype.ngOnInit = function () {
    };
    TopCallsComponent.prototype.getTopCallsByDuration = function (howMany) {
        var _this = this;
        this.topCallsByDurationService.getTopCallsByDuration(howMany).subscribe(function (extensions) {
            _this.extensions = extensions;
        });
        this.initDataSource();
    };
    TopCallsComponent.prototype.initDataSource = function () {
        this.dataSource = new _util_TopCallsDataSource__WEBPACK_IMPORTED_MODULE_1__["TopCallsDataSource"](this.extensions, this.sort);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"])
    ], TopCallsComponent.prototype, "sort", void 0);
    TopCallsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-top-calls',
            template: __webpack_require__(/*! ./top-calls.component.html */ "./src/app/components/top-calls/top-calls/top-calls.component.html"),
            styles: [__webpack_require__(/*! ./top-calls.component.css */ "./src/app/components/top-calls/top-calls/top-calls.component.css")]
        }),
        __metadata("design:paramtypes", [_services_top_calls_data_top_calls_by_duration_service__WEBPACK_IMPORTED_MODULE_2__["TopCallsByDurationService"]])
    ], TopCallsComponent);
    return TopCallsComponent;
}());



/***/ }),

/***/ "./src/app/models/DbUser.ts":
/*!**********************************!*\
  !*** ./src/app/models/DbUser.ts ***!
  \**********************************/
/*! exports provided: DbUser */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DbUser", function() { return DbUser; });
var DbUser = /** @class */ (function () {
    function DbUser() {
        this._username = '';
        this._password = '';
    }
    Object.defineProperty(DbUser.prototype, "username", {
        get: function () {
            return this._username;
        },
        set: function (value) {
            this._username = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DbUser.prototype, "password", {
        get: function () {
            return this._password;
        },
        set: function (value) {
            this._password = value;
        },
        enumerable: true,
        configurable: true
    });
    DbUser.prototype.toJSON = function () {
        var obj = Object.assign(this);
        var keys = Object.keys(this.constructor.prototype);
        obj.toJSON = undefined;
        return JSON.stringify(obj, keys);
    };
    return DbUser;
}());



/***/ }),

/***/ "./src/app/services/extension-detailed-data/extension-detailed-data.service.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/services/extension-detailed-data/extension-detailed-data.service.ts ***!
  \*************************************************************************************/
/*! exports provided: ExtensionDetailedDataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExtensionDetailedDataService", function() { return ExtensionDetailedDataService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ExtensionDetailedDataService = /** @class */ (function () {
    function ExtensionDetailedDataService(http) {
        this.http = http;
    }
    ExtensionDetailedDataService.prototype.getBySrc = function (src, allowInternal) {
        return this.http.get('http://localhost:8081/reports/detailed?src=' + src + '&allowInternal=' + allowInternal);
    };
    ExtensionDetailedDataService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ExtensionDetailedDataService);
    return ExtensionDetailedDataService;
}());



/***/ }),

/***/ "./src/app/services/extension-summary-data/extension-summary-data.service.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/services/extension-summary-data/extension-summary-data.service.ts ***!
  \***********************************************************************************/
/*! exports provided: ExtensionSummaryDataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExtensionSummaryDataService", function() { return ExtensionSummaryDataService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ExtensionSummaryDataService = /** @class */ (function () {
    function ExtensionSummaryDataService(http) {
        this.http = http;
    }
    ExtensionSummaryDataService.prototype.getAllExtensions = function () {
        return this.http.get('http://localhost:8081/reports/summary');
    };
    ExtensionSummaryDataService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ExtensionSummaryDataService);
    return ExtensionSummaryDataService;
}());



/***/ }),

/***/ "./src/app/services/login-technical-data/login-technical-data.service.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/services/login-technical-data/login-technical-data.service.ts ***!
  \*******************************************************************************/
/*! exports provided: LoginTechnicalDataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginTechnicalDataService", function() { return LoginTechnicalDataService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};


var LoginTechnicalDataService = /** @class */ (function () {
    function LoginTechnicalDataService(http) {
        this.http = http;
    }
    LoginTechnicalDataService.prototype.uploadFileTest = function (formData) {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log('test');
                        return [4 /*yield*/, this.http.post('http://localhost:8081/login/technical/file-upload', formData).toPromise()];
                    case 1:
                        response = _a.sent();
                        return [2 /*return*/, response];
                }
            });
        });
    };
    LoginTechnicalDataService.prototype.uploadDetails = function (dbUser, nameAndPins) {
        console.log(dbUser);
        console.log(nameAndPins);
        var finalJSON;
        finalJSON = '{ "dbUser": ' + dbUser +
            ', "nameAndPinCode": ' + nameAndPins + '}';
        console.log(finalJSON);
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('Content-Type', 'application/json');
        this.http.post('http://localhost:8081/login/updateDbDetails', finalJSON, { headers: headers }).subscribe();
    };
    LoginTechnicalDataService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], LoginTechnicalDataService);
    return LoginTechnicalDataService;
}());



/***/ }),

/***/ "./src/app/services/pin-code-data/pin-code-data.service.ts":
/*!*****************************************************************!*\
  !*** ./src/app/services/pin-code-data/pin-code-data.service.ts ***!
  \*****************************************************************/
/*! exports provided: PinCodeDataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PinCodeDataService", function() { return PinCodeDataService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PinCodeDataService = /** @class */ (function () {
    function PinCodeDataService(http) {
        this.http = http;
    }
    PinCodeDataService.prototype.getByPinCode = function (pinCode, allowInternal) {
        console.log(pinCode);
        console.log('http://localhost:8081/reports/pincode?pinCcode=' + pinCode + '&allowInternal=' + allowInternal);
        return this.http.get('http://localhost:8081/reports/pincode?pinCode=' + pinCode + '&allowInternal=' + allowInternal);
    };
    PinCodeDataService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], PinCodeDataService);
    return PinCodeDataService;
}());



/***/ }),

/***/ "./src/app/services/top-calls-data/top-calls-by-duration.service.ts":
/*!**************************************************************************!*\
  !*** ./src/app/services/top-calls-data/top-calls-by-duration.service.ts ***!
  \**************************************************************************/
/*! exports provided: TopCallsByDurationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TopCallsByDurationService", function() { return TopCallsByDurationService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TopCallsByDurationService = /** @class */ (function () {
    function TopCallsByDurationService(http) {
        this.http = http;
    }
    TopCallsByDurationService.prototype.getTopCallsByDuration = function (howManyCalls) {
        return this.http.get('http://localhost:8081/reports/top-calls/duration?howManyCalls=' + howManyCalls);
    };
    TopCallsByDurationService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], TopCallsByDurationService);
    return TopCallsByDurationService;
}());



/***/ }),

/***/ "./src/app/util/ExtensionDetailedDataSource.ts":
/*!*****************************************************!*\
  !*** ./src/app/util/ExtensionDetailedDataSource.ts ***!
  \*****************************************************/
/*! exports provided: ExtensionDetailedDataSource */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExtensionDetailedDataSource", function() { return ExtensionDetailedDataSource; });
/* harmony import */ var _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/cdk/collections */ "./node_modules/@angular/cdk/esm5/collections.es5.js");
/* harmony import */ var rxjs_Observable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/Observable */ "./node_modules/rxjs-compat/_esm5/Observable.js");
/* harmony import */ var rxjs_compat_add_observable_of__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs-compat/add/observable/of */ "./node_modules/rxjs-compat/add/observable/of.js");
/* harmony import */ var rxjs_compat_add_observable_of__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(rxjs_compat_add_observable_of__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var rxjs_compat_add_observable_merge__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs-compat/add/observable/merge */ "./node_modules/rxjs-compat/add/observable/merge.js");
/* harmony import */ var rxjs_compat_add_observable_merge__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(rxjs_compat_add_observable_merge__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();





var ExtensionDetailedDataSource = /** @class */ (function (_super) {
    __extends(ExtensionDetailedDataSource, _super);
    function ExtensionDetailedDataSource(extensions, sort) {
        var _this = _super.call(this) || this;
        _this.extensions = extensions;
        _this.sort = sort;
        return _this;
    }
    ExtensionDetailedDataSource.prototype.connect = function () {
        var _this = this;
        if (this.extensions === null || this.extensions === []) {
            this.extensions = [];
            return rxjs_Observable__WEBPACK_IMPORTED_MODULE_1__["Observable"].of(this.extensions);
        }
        else {
            var dataChanges = [
                this.extensions,
                this.sort.sortChange
            ];
            return rxjs_Observable__WEBPACK_IMPORTED_MODULE_1__["Observable"].merge.apply(rxjs_Observable__WEBPACK_IMPORTED_MODULE_1__["Observable"], dataChanges).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function () {
                var sortedData = _this.sortData();
                return sortedData;
            }));
        }
    };
    ExtensionDetailedDataSource.prototype.sortData = function () {
        var _this = this;
        var sortedData = this.extensions.slice();
        if (!this.sort.active || this.sort.direction === '') {
            return sortedData;
        }
        return sortedData.sort(function (a, b) {
            var isAsc = _this.sort.direction === 'asc';
            switch (_this.sort.active) {
                case 'callDate':
                    return _this.compare(a.callDate, b.callDate, isAsc);
                case 'clid':
                    return _this.compare(+a.clid, +b.clid, isAsc);
                case 'src':
                    return _this.compare(+a.src, +b.src, isAsc);
                case 'destination':
                    return _this.compare(+a.destination, +b.destination, isAsc);
                case 'duration':
                    return _this.compare(+a.duration, +b.duration, isAsc);
                case 'cost':
                    return _this.compare(+a.cost, +b.cost, isAsc);
                default:
                    return 0;
            }
        });
    };
    ExtensionDetailedDataSource.prototype.compare = function (a, b, isAsc) {
        return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
    };
    ExtensionDetailedDataSource.prototype.disconnect = function () {
    };
    return ExtensionDetailedDataSource;
}(_angular_cdk_collections__WEBPACK_IMPORTED_MODULE_0__["DataSource"]));



/***/ }),

/***/ "./src/app/util/ExtensionSummaryDataSource.ts":
/*!****************************************************!*\
  !*** ./src/app/util/ExtensionSummaryDataSource.ts ***!
  \****************************************************/
/*! exports provided: ExtensionSummaryDataSource */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExtensionSummaryDataSource", function() { return ExtensionSummaryDataSource; });
/* harmony import */ var _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/cdk/collections */ "./node_modules/@angular/cdk/esm5/collections.es5.js");
/* harmony import */ var rxjs_Observable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/Observable */ "./node_modules/rxjs-compat/_esm5/Observable.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs_compat_add_observable_of__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs-compat/add/observable/of */ "./node_modules/rxjs-compat/add/observable/of.js");
/* harmony import */ var rxjs_compat_add_observable_of__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(rxjs_compat_add_observable_of__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var rxjs_compat_add_observable_merge__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs-compat/add/observable/merge */ "./node_modules/rxjs-compat/add/observable/merge.js");
/* harmony import */ var rxjs_compat_add_observable_merge__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(rxjs_compat_add_observable_merge__WEBPACK_IMPORTED_MODULE_4__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();





var ExtensionSummaryDataSource = /** @class */ (function (_super) {
    __extends(ExtensionSummaryDataSource, _super);
    function ExtensionSummaryDataSource(extensions, sort) {
        var _this = _super.call(this) || this;
        _this.extensions = extensions;
        _this.sort = sort;
        return _this;
    }
    ExtensionSummaryDataSource.prototype.connect = function () {
        var _this = this;
        if (this.extensions === null || this.extensions === []) {
            this.extensions = [];
            return rxjs_Observable__WEBPACK_IMPORTED_MODULE_1__["Observable"].of(this.extensions);
        }
        else {
            var dataChanges = [
                this.extensions,
                this.sort.sortChange
            ];
            return rxjs_Observable__WEBPACK_IMPORTED_MODULE_1__["Observable"].merge.apply(rxjs_Observable__WEBPACK_IMPORTED_MODULE_1__["Observable"], dataChanges).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function () {
                var sortedData = _this.sortData();
                return sortedData;
            }));
        }
    };
    ExtensionSummaryDataSource.prototype.sortData = function () {
        var _this = this;
        var sortedData = this.extensions.slice();
        if (!this.sort.active || this.sort.direction === '') {
            return sortedData;
        }
        return sortedData.sort(function (a, b) {
            var isAsc = _this.sort.direction === 'asc';
            switch (_this.sort.active) {
                case 'clid':
                    return _this.compare(a.clid, b.clid, isAsc);
                case 'src':
                    return _this.compare(+a.src, +b.src, isAsc);
                case 'duration':
                    return _this.compare(+a.duration, +b.duration, isAsc);
                case 'cost':
                    return _this.compare(+a.cost, +b.cost, isAsc);
                case 'numOfCalls':
                    return _this.compare(+a.numOfCalls, +b.numOfCalls, isAsc);
                default:
                    return 0;
            }
        });
    };
    ExtensionSummaryDataSource.prototype.compare = function (a, b, isAsc) {
        return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
    };
    ExtensionSummaryDataSource.prototype.disconnect = function () {
    };
    return ExtensionSummaryDataSource;
}(_angular_cdk_collections__WEBPACK_IMPORTED_MODULE_0__["DataSource"]));



/***/ }),

/***/ "./src/app/util/PinCodeDataSource.ts":
/*!*******************************************!*\
  !*** ./src/app/util/PinCodeDataSource.ts ***!
  \*******************************************/
/*! exports provided: PinCodeDataSource */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PinCodeDataSource", function() { return PinCodeDataSource; });
/* harmony import */ var rxjs_Observable__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/Observable */ "./node_modules/rxjs-compat/_esm5/Observable.js");
/* harmony import */ var _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/cdk/collections */ "./node_modules/@angular/cdk/esm5/collections.es5.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs_compat_add_observable_of__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs-compat/add/observable/of */ "./node_modules/rxjs-compat/add/observable/of.js");
/* harmony import */ var rxjs_compat_add_observable_of__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(rxjs_compat_add_observable_of__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var rxjs_compat_add_observable_merge__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs-compat/add/observable/merge */ "./node_modules/rxjs-compat/add/observable/merge.js");
/* harmony import */ var rxjs_compat_add_observable_merge__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(rxjs_compat_add_observable_merge__WEBPACK_IMPORTED_MODULE_4__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();





var PinCodeDataSource = /** @class */ (function (_super) {
    __extends(PinCodeDataSource, _super);
    function PinCodeDataSource(extensions, sort) {
        var _this = _super.call(this) || this;
        _this.extensions = extensions;
        _this.sort = sort;
        return _this;
    }
    PinCodeDataSource.prototype.connect = function () {
        var _this = this;
        if (this.extensions === null || this.extensions === []) {
            this.extensions = [];
            return rxjs_Observable__WEBPACK_IMPORTED_MODULE_0__["Observable"].of(this.extensions);
        }
        else {
            var dataChanges = [
                this.extensions,
                this.sort.sortChange
            ];
            return rxjs_Observable__WEBPACK_IMPORTED_MODULE_0__["Observable"].merge.apply(rxjs_Observable__WEBPACK_IMPORTED_MODULE_0__["Observable"], dataChanges).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function () {
                var sortedData = _this.sortData();
                return sortedData;
            }));
        }
    };
    PinCodeDataSource.prototype.sortData = function () {
        var _this = this;
        var sortedData = this.extensions.slice();
        if (!this.sort.active || this.sort.direction === '') {
            return sortedData;
        }
        return sortedData.sort(function (a, b) {
            var isAsc = _this.sort.direction === 'asc';
            switch (_this.sort.active) {
                case 'clid':
                    return _this.compare(a.clid, b.clid, isAsc);
                case 'src':
                    return _this.compare(+a.src, +b.src, isAsc);
                case 'destination':
                    return _this.compare(+a.destination, +b.destination, isAsc);
                case 'duration':
                    return _this.compare(+a.duration, +b.duration, isAsc);
                case 'cost':
                    return _this.compare(+a.cost, +b.cost, isAsc);
                default:
                    return 0;
            }
        });
    };
    PinCodeDataSource.prototype.compare = function (a, b, isAsc) {
        return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
    };
    PinCodeDataSource.prototype.disconnect = function () {
    };
    return PinCodeDataSource;
}(_angular_cdk_collections__WEBPACK_IMPORTED_MODULE_1__["DataSource"]));



/***/ }),

/***/ "./src/app/util/TopCallsDataSource.ts":
/*!********************************************!*\
  !*** ./src/app/util/TopCallsDataSource.ts ***!
  \********************************************/
/*! exports provided: TopCallsDataSource */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TopCallsDataSource", function() { return TopCallsDataSource; });
/* harmony import */ var _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/cdk/collections */ "./node_modules/@angular/cdk/esm5/collections.es5.js");
/* harmony import */ var rxjs_Observable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/Observable */ "./node_modules/rxjs-compat/_esm5/Observable.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs_compat_add_observable_of__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs-compat/add/observable/of */ "./node_modules/rxjs-compat/add/observable/of.js");
/* harmony import */ var rxjs_compat_add_observable_of__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(rxjs_compat_add_observable_of__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var rxjs_compat_add_observable_merge__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs-compat/add/observable/merge */ "./node_modules/rxjs-compat/add/observable/merge.js");
/* harmony import */ var rxjs_compat_add_observable_merge__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(rxjs_compat_add_observable_merge__WEBPACK_IMPORTED_MODULE_4__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();





var TopCallsDataSource = /** @class */ (function (_super) {
    __extends(TopCallsDataSource, _super);
    function TopCallsDataSource(extensions, sort) {
        var _this = _super.call(this) || this;
        _this.extensions = extensions;
        _this.sort = sort;
        return _this;
    }
    TopCallsDataSource.prototype.connect = function () {
        var _this = this;
        if (this.extensions === null || this.extensions === []) {
            this.extensions = [];
            return rxjs_Observable__WEBPACK_IMPORTED_MODULE_1__["Observable"].of(this.extensions);
        }
        else {
            var dataChanges = [
                this.extensions,
                this.sort.sortChange
            ];
            return rxjs_Observable__WEBPACK_IMPORTED_MODULE_1__["Observable"].merge.apply(rxjs_Observable__WEBPACK_IMPORTED_MODULE_1__["Observable"], dataChanges).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function () {
                var sortedData = _this.sortData();
                return sortedData;
            }));
        }
    };
    TopCallsDataSource.prototype.sortData = function () {
        var _this = this;
        var sortedData = this.extensions.slice();
        if (!this.sort.active || this.sort.direction === '') {
            return sortedData;
        }
        return sortedData.sort(function (a, b) {
            var isAsc = _this.sort.direction === 'asc';
            switch (_this.sort.active) {
                case 'callDate':
                    return _this.compare(a.callDate, b.callDate, isAsc);
                case 'clid':
                    return _this.compare(+a.clid, +b.clid, isAsc);
                case 'src':
                    return _this.compare(+a.src, +b.src, isAsc);
                case 'destination':
                    return _this.compare(+a.destination, +b.destination, isAsc);
                case 'duration':
                    return _this.compare(+a.duration, +b.duration, isAsc);
                case 'cost':
                    return _this.compare(+a.cost, +b.cost, isAsc);
                default:
                    return 0;
            }
        });
    };
    TopCallsDataSource.prototype.compare = function (a, b, isAsc) {
        return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
    };
    TopCallsDataSource.prototype.disconnect = function () {
    };
    return TopCallsDataSource;
}(_angular_cdk_collections__WEBPACK_IMPORTED_MODULE_0__["DataSource"]));



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\Work\FPComms\ReportingTool\reporting-tool-parent\reportingtoolparent\reportingtoolapp\web\angular-app\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map