//package co.za.fpcomms.reportingtooljava;
//
//import co.za.fpcomms.reportingtooljava.entities.CdrDurationEntity;
//import co.za.fpcomms.reportingtooljava.entities.CdrEntity;
//import co.za.fpcomms.reportingtooljava.repositories.CdrDurationRepository;
//import co.za.fpcomms.reportingtooljava.repositories.CdrRepository;
////import co.za.fpcomms.reportingtooljava.util.CustomComparator;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Iterator;
//import java.util.List;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//public class CdrService {
//
//    @Autowired
//    private CdrRepository cdrRepository;
//
//    @Autowired
//    private CdrDurationRepository cdrDurationRepository;
//
//    private List<CdrEntity> summaryList = new ArrayList<>();
//
////    @Test
////    public void getAllCdrRecords() {
////        List<CdrEntity> dbList = cdrRepository.findAll();
////        List<String> extList = new ArrayList<>();
////
////        Iterator itr = dbList.iterator();
////
////        while (itr.hasNext()) {
////            CdrEntity ext = (CdrEntity) itr.next();
////
////            // if extension is not in list
////            if (!(extList.contains(ext.getSrc()))) {
////                extList.add(ext.getSrc());
////                summaryList.add(ext);
////                ext.setNumOfCalls(ext.getNumOfCalls() + 1L);
////
////            } else { //if extension is in list
////                sumUpColumns(ext, extList.indexOf(ext.getSrc()));
////                if ( extList.indexOf(ext.getSrc()) == 10) {
////                    System.out.println(ext.getDuration() + "------");
////                }
////            }
////        }
////        System.out.println(summaryList.get(10).getDuration() + " -- " + summaryList.get(10).getNumOfCalls());
////    }
//
////    public void sumUpColumns(CdrEntity ext, int index) {
////
////        CdrEntity extensionToEdit = summaryList.get(index);
////
////        int duration = extensionToEdit.getDuration();
////        int durationToAdd = ext.getDuration();
////        Long totalCalls = extensionToEdit.getNumOfCalls();
////
////        extensionToEdit.setNumOfCalls(totalCalls + 1L);
////
////        extensionToEdit.setDuration(duration + durationToAdd);
////        if (index == 10) {
////            System.out.println(duration + "--" + durationToAdd);
////        }
////
////    }
//
//    @Test
//    public void getTopCallsByDuration(){
//        List<CdrDurationEntity> dbList;
//        dbList = cdrDurationRepository.findAll();
//
//        List<CdrDurationEntity> listToSort = new ArrayList<>();
//
//        Iterator itr = dbList.iterator();
//
//        while (itr.hasNext()) {
//            CdrDurationEntity call = (CdrDurationEntity) itr.next();
//            listToSort.add(call);
//        }
//
//        Collections.sort(listToSort);
//        Iterator itr2 = listToSort.iterator();
//
//        List<CdrDurationEntity> sortedTopCallsList = new ArrayList<>();
//
//        for (int i = 0; i < 40; i++) {
//            CdrDurationEntity call = (CdrDurationEntity) itr2.next();
//            sortedTopCallsList.add(call);
//            System.out.println(call.getDuration());
//        }
//    }
//
//    // CONVERT TIME TO HH:MM:SS
//    @Test
//    public void convertTime() {
//        int totalDuration = Integer.parseInt("7268");
//
//        int hours = totalDuration / 3600;
//        int mins = (totalDuration % 3600) / 60;
//        int secs = totalDuration % 60;
//
//        System.out.println(String.format("%02d:%02d:%02d", hours, mins, secs));
//    }
//
//}
