package co.za.fpcomms.reportingtooljava;

import co.za.fpcomms.reportingtooljava.entities.CostEntity;
import co.za.fpcomms.reportingtooljava.repositories.CostRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LoginService {

    @Autowired
    CostRepository costRepository;

    @Test
    public void deleteAll(){
        List<CostEntity> list = costRepository.findAll();
        costRepository.deleteAll(list);
    }

}
